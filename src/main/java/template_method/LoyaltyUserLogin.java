package template_method;

public class LoyaltyUserLogin extends BaseLogin {
    @Override
    public boolean isUserRegistered() {
        return true;
    }

    @Override
    public boolean isUserLoyalty() {
        return true;
    }
}
