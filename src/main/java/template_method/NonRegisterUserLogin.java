package template_method;

public class NonRegisterUserLogin extends BaseLogin {
    @Override
    public boolean isUserRegistered() {
        return false;
    }

    @Override
    public boolean isUserLoyalty() {
        return false;
    }
}
