package template_method;

public class RegisteredUserLogin extends BaseLogin{
    @Override
    public boolean isUserRegistered() {
        return true;
    }

    @Override
    public boolean isUserLoyalty() {
        return false;
    }
}
