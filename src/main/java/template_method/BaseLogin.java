package template_method;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseLogin {

    protected static Map<String, String> userCredential = new HashMap<>();

    public void login(Map<String, String> userCredential) {
        if (!isUserRegistered()) {
            registerUser("new user login", "new user password");
        } else {
            userCredential.put("login","old login");
            userCredential.put("password","old password");
        }
        if(isUserLoyalty()){
            String loyaltyNumber = RandomStringUtils.randomAlphanumeric(10);
            userCredential.put("loyaltyNumber", loyaltyNumber);
        }
        enterCredential(userCredential);
    }

    public abstract boolean isUserRegistered();

    public abstract boolean isUserLoyalty();

    public Map<String, String> registerUser(String login, String password) {
        userCredential.put("login", login);
        userCredential.put("password", password);
        return userCredential;
    }

    public void enterCredential(Map<String, String> userCredential) {
        System.out.println("Enter login: " + userCredential.get("login"));
        System.out.println("Enter password: " + userCredential.get("password"));
        if (isUserLoyalty()) {
            System.out.println("Enter ffn: " + userCredential.get("loyaltyNumber"));
        }
    }

    public static void main(String[] args) {
        RegisteredUserLogin loyaltyUserLogin = new RegisteredUserLogin();
        loyaltyUserLogin.login(userCredential);
    }
}
