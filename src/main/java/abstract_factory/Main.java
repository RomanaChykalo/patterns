package abstract_factory;

import abstract_factory.entity.Color;
import abstract_factory.factory.DarkSchemeFactory;
import abstract_factory.factory.SchemeFactory;

public class Main {
    public static void main(String[] args) {
        SchemeFactory darkSchemeFactory = new DarkSchemeFactory();
        darkSchemeFactory.initScheme(Color.RED);
    }
}
