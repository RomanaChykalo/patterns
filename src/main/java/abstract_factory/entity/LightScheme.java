package abstract_factory.entity;

public class LightScheme extends Scheme {
    public LightScheme(Font font, Background background) {
        super(font, background);
    }
}
