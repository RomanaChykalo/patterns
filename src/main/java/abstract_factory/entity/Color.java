package abstract_factory.entity;

public enum Color {
    BLACK,
    WHITE,
    RED,
    GREEN
}
