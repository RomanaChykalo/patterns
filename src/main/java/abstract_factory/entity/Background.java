package abstract_factory.entity;

public class Background {
    private Color color;

    public Background(Color color){
        this.color=color;
    }

    public Color getColor() {
        return color;
    }
}
