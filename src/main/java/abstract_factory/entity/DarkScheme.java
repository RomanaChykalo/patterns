package abstract_factory.entity;

public class DarkScheme extends Scheme {
    public DarkScheme(Font font, Background background) {
        super(font, background);
    }
}
