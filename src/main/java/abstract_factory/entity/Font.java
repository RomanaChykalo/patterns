package abstract_factory.entity;

import abstract_factory.entity.Color;

public class Font {
    private Color color;
    private int size;
    private String family;

    public Font(Color color, int size, String family) {
        this.color = color;
        this.size = size;
        this.family = family;
    }

    public Color getColor() {
        return color;
    }

    public int getSize() {
        return size;
    }

    public String getFamily() {
        return family;
    }
}
