package abstract_factory.entity;

import abstract_factory.entity.Background;
import abstract_factory.entity.Font;

public class Scheme {
    protected Font font;
    protected Background background;

    public Scheme(Font font, Background background) {
        this.font = font;
        this.background = background;
    }
}
