package abstract_factory.factory;

import abstract_factory.entity.Color;
import abstract_factory.entity.Scheme;

public interface SchemeFactory {
    Scheme initScheme(Color textColor);
}
