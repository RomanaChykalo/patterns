package abstract_factory.factory;

import abstract_factory.entity.*;

import java.util.Scanner;

public class LightSchemeFactory implements SchemeFactory {

    @Override
    public Scheme initScheme(Color textColor) {
        if (textColor.equals(Color.WHITE)) {
            System.out.println("Can't have white text on light background");
            Scanner scanner = new Scanner(System.in);
            textColor = Color.valueOf(scanner.nextLine());
            initScheme(textColor);
        }
        System.out.println("Create light scheme with textcolor:..." + textColor);
        return new LightScheme(new Font(textColor, 32, "New time roman"), new Background(Color.WHITE));
    }
}
