package abstract_factory.factory;

import abstract_factory.entity.*;

import java.util.Scanner;

public class DarkSchemeFactory implements SchemeFactory {
    @Override
    public Scheme initScheme(Color textColor) {
        if (textColor.equals(Color.BLACK)) {
            System.out.println("Can't have black text on light background");
            Scanner scanner = new Scanner(System.in);
            textColor = Color.valueOf(scanner.nextLine());
            initScheme(textColor);
        }
        System.out.println("Create dark scheme with textcolor:..." + textColor);
        return new DarkScheme(new Font(textColor, 32, "New time roman"), new Background(Color.BLACK));
    }
}
